import { BROWSER, ICONS } from './constants';

export class CustomIcon {
    public fill: string = 'none';
    public number: string = '';
    public stroke: string = 'currentColor';
    public size: number = 24;
    public strokeWidth: number = 2;
    public numberIsVisible: boolean = true;

    private svg: string = '';

    // ---
    // PUBLIC
    // ---

    public toSvg(): string {
        let number: string;
        let y: string;

        (this.numberIsVisible) ? number = this.number : number = '';
        (BROWSER === 'Chrome' || BROWSER === 'Safari') ? y = '12' : y = '16';

        return '<svg xmlns="http://www.w3.org/2000/svg"'
            + 'width="' + this.size + '"'
            + 'height="' + this.size + '"'
            + 'stroke="' + this.stroke + '"'
            + 'fill="' + this.fill + '"'

            + 'viewBox="0 0 24 24"'
            + 'stroke-width="' + this.strokeWidth + '"'
            + 'stroke-linecap="round"'
            + 'stroke-linejoin="round"'
        + '>'
            + this.svg
            + '<text text-anchor="middle" alignment-baseline="central" x="12" y="' + y + '" stroke="' + this.fill + '" stroke-width="3">'
                + number
            + '</text>'
            + '<text text-anchor="middle" alignment-baseline="central" x="12" y="' + y + '" fill="' + this.stroke + '"stroke-width="1">'
                + number
            + '</text>'
        + '</svg>';
    }

    public setFill(color: string): CustomIcon {
        if (color !== undefined) {
            this.fill = color;
        }
        return this;
    }

    public setIcon(name: string): CustomIcon {
        if (name !== undefined) {
            this.svg = ICONS[name];
        }
        return this;
    }

    public setNumber(number: string): CustomIcon {
        if (number !== undefined) {
            this.number = number;
        }
        return this;
    }

    public setSize(size: number): CustomIcon {
        if (size !== undefined) {
            this.size = size;
        }
        return this;
    }

    public setStroke(color: string): CustomIcon {
        if (color !== undefined) {
            this.stroke = color;
        }
        return this;
    }

    public setStrokeWidth(size: number): CustomIcon {
        if (size !== undefined) {
            this.strokeWidth = size;
        }
        return this;
    }

    public toggleNumber(): CustomIcon {
        this.numberIsVisible = ! this.numberIsVisible;
        return this;
    }
}
