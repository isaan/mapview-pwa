import * as L from 'leaflet';
import { COLORS } from './constants';
import { blendColors } from './blender';
import { CustomMarker } from './custom-marker';

export class CustomPolyline {
    public _leafletLine: L.Polyline;

    private marker1: CustomMarker;
    private marker2: CustomMarker;
    private color: string;
    private isVisible: boolean;

    constructor(marker1: CustomMarker, marker2: CustomMarker) {
        this.color = COLORS.WHITE;
        this._leafletLine = new L.Polyline([marker1.getLatLng(), marker2.getLatLng()], {
            color: this.color,
            weight: 5,
            opacity: 1,
            smoothFactor: 1,
        });

        this.marker1 = marker1;
        this.marker2 = marker2;
        this.marker1.onDrag(() => {
            this._leafletLine.setLatLngs([this.marker1.getLatLng(), this.marker2.getLatLng()]);
        });
        this.marker2.onDrag(() => {
            this._leafletLine.setLatLngs([this.marker1.getLatLng(), this.marker2.getLatLng()]);
        });

        this.isVisible = true;
    }

    // ---
    // PUBLIC
    // ---

    public addTo(map: L.Map): void {
        this._leafletLine.addTo(map);
    }

    public removeFrom(map: L.Map): void {
        map.removeLayer(this._leafletLine);
    }

    public toggle(): void {
        if (this.isVisible) {
            this._leafletLine.setStyle({opacity: 0});
            this.isVisible = false;
        } else {
            this._leafletLine.setStyle({opacity: 1});
            this.isVisible = true;
        }
    }

    public updateColor(lowLimit: number, lowLimitColor: string, highLimit: number, highLimitColor: string): void {
        const value1: number | undefined = this.marker1.value;
        const value2: number | undefined = this.marker2.value;

        if (value1 !== undefined && value2 !== undefined) {
            const value: number = (value1 + value2) / 2;

            if (value > lowLimit) {
                if (value > highLimit) {
                    this.color = highLimitColor;
                } else {
                    this.color = blendColors(lowLimitColor, highLimitColor, 0.5);
                }
            } else {
                this.color = lowLimitColor;
            }
        }

        this._leafletLine.setStyle({color: this.color});
    }
}
