import * as L from 'leaflet';
import { COLORS } from './constants';
import { CustomImage } from './custom-image';
import { CustomMarker } from './custom-marker';

export class CustomImages {
    private images: CustomImage[];

    constructor() {
        this.images = new Array();
    }

    // ---
    // PUBLIC
    // ---

    public addImage(map: L.Map, dataUrl: string, topleft: L.LatLngExpression, topright: L.LatLngExpression, bottomleft: L.LatLngExpression, doBound: boolean): void {
        const marker1: CustomMarker = new CustomMarker(topleft, 'IconCircle', COLORS.BLACK);
        const marker2: CustomMarker = new CustomMarker(topright, 'IconCircle', COLORS.BLACK);
        const marker3: CustomMarker = new CustomMarker(bottomleft, 'IconCircle', COLORS.BLACK);

        marker1.addTo(map);
        marker2.addTo(map);
        marker3.addTo(map);

        if (doBound) {
            const bounds: L.LatLngBounds = new L.LatLngBounds(topleft, topright).extend(bottomleft);
            map.fitBounds(bounds, { animate: false });
        }

        const imageOverlay: CustomImage = new CustomImage(marker1, marker2, marker3, dataUrl);
        imageOverlay.addTo(map);
        this.images.push(imageOverlay);
    }

    public clean(map: L.Map): void {
        this.images.forEach((image: CustomImage) => {
            map.removeLayer(image._leafletImage);
            map.removeLayer(image.markers[0]._leafletMarker);
            map.removeLayer(image.markers[1]._leafletMarker);
            map.removeLayer(image.markers[2]._leafletMarker);
        });
        this.images = new Array();
    }

    public toggle(): void {
        this.images.forEach((image: CustomImage) => {
            image.toggle();
        });
    }
}
