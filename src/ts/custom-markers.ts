import * as L from 'leaflet';
import { COLORS } from './constants';
import { CustomMarker } from './custom-marker';
import { CustomPolylines } from './custom-polylines';

export class CustomMarkers {
    public name: string;

    private _lines: CustomPolylines;
    private markers: CustomMarker[];

    constructor(name: string) {
        this._lines = new CustomPolylines();
        this.markers = new Array();
        this.name = name;
    }

    // ---
    // PUBLIC
    // ---

    public addMarker(map: L.Map, icon: any, coord: any, dValues?: number[]): void {
        const marker: CustomMarker = new CustomMarker(coord, icon, COLORS.WHITE, dValues);

        if (this.name !== 'loose') {
            marker.setNumber(this.markers.length + 1);
        }

        marker.addTo(map);
        this.markers.push(marker);

        this.addLine(map);
    }

    public clean(map: L.Map): void {
        this.markers.forEach((marker: CustomMarker) => {
            map.removeLayer(marker._leafletMarker);
        });
        this.markers = new Array();
        this._lines.clean(map);
    }

    public unselect(): void {
        this.markers.forEach((marker: CustomMarker) => {
            // marker.unselect();
        });
    }

    public toggleLines(): void {
        this._lines.toggle();
    }

    public toggleMarkers(): void {
        this.markers.forEach((marker: CustomMarker) => {
            marker.toggle();
        });
    }

    public toggleNumbers(): void {
        this.markers.forEach((marker: CustomMarker) => {
            marker.toggleNumber();
        });
    }

    public updateColors(lowLimit: number, lowLimitColor: string, highLimit: number, highLimitColor: string): void {
        this.markers.forEach((marker: CustomMarker) => {
            marker.updateColor(lowLimit, lowLimitColor, highLimit, highLimitColor);
        });
        this._lines.updateColors(lowLimit, lowLimitColor, highLimit, highLimitColor);
    }

    // ---
    // GETTERS
    // ---

    public getLength(): number {
        return this.markers.length;
    }

    // ---
    // PRIVATE
    // ---

    private addLine(map: L.Map): void {
        if (this.markers.length > 1) {
            const marker1: CustomMarker = this.markers[this.markers.length - 2];
            const marker2: CustomMarker = this.markers[this.markers.length - 1];
            this._lines.addLine(map, marker1, marker2);
        }
    }
}
