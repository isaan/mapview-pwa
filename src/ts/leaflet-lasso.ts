import * as L from 'leaflet';
import 'leaflet.markercluster';
import { LeafletEvent, LeafletMouseEvent } from 'leaflet';
import booleanPointInPolygon from '@turf/boolean-point-in-polygon';

declare module 'leaflet' {
    export interface LassoOptions {
        rectangle?: PolylineOptions;
        cursor?: string;
    }

    export interface LassoFinishedEvent extends LeafletEvent {
        latLngs: LatLng[];
        layers: Layer[];
    }

    export class Lasso extends Handler {
        constructor(map: Map, options?: LassoOptions);
    }

    export const lasso: (map: Map, options?: LassoOptions) => Lasso;
}

const lasso = L.Handler.extend({
    options: {
        rectangle: {
            color: '#FFFFFF',
            weight: 4,
        },
        cursor: 'crosshair',
    } as L.LassoOptions,

    map: undefined as L.Map | undefined,
    rectangle: undefined as L.Rectangle | undefined,
    firstPointLatLng: undefined as [number, number] | undefined,
    firstPointPixel: undefined as [number, number] | undefined,
    lastPointLatLng: undefined as [number, number] | undefined,
    lastPointPixel: undefined as [number, number] | undefined,

    initialize(map: L.Map, options?: L.LassoOptions) {
        this.map = map;
        this.onMouseUpBound = this.onMouseUp.bind(this);
        L.Util.setOptions(this, options);
    },

    addHooks() {
        this.map.on('mousedown', this.onMouseDown, this);
        this.map.on('mouseup', this.onMouseUp, this);
        document.addEventListener('mouseup', this.onMouseUpBound, true);

        const mapContainer = this.map.getContainer();
        mapContainer.style.cursor = this.options.cursor || '';
        mapContainer.style.userSelect = 'none';
        mapContainer.style.msUserSelect = 'none';
        mapContainer.style.mozUserSelect = 'none';
        mapContainer.style.webkitUserSelect = 'none';

        this.map.dragging.disable();
        this.map.fire('lasso.enabled');
    },

    removeHooks() {
        this.map.off('mousedown', this.onMouseDown, this);
        this.map.off('mousemove', this.onMouseMove, this);
        this.map.off('mouseup', this.onMouseUp, this);
        document.removeEventListener('mouseup', this.onMouseUpBound);

        const mapContainer = this.map.getContainer();
        mapContainer.style.cursor = '';
        mapContainer.style.userSelect = '';
        mapContainer.style.msUserSelect = '';
        mapContainer.style.mozUserSelect = '';
        mapContainer.style.webkitUserSelect = '';

        this.map.dragging.enable();
        this.map.fire('lasso.disabled');
    },

    onMouseDown(event: LeafletEvent) {
        if (!this.rectangle) {
            const event2 = event as LeafletMouseEvent;
            this.firstPointLatLng = [event2.latlng.lat, event2.latlng.lng];
            this.firstPointPixel = [event2.containerPoint.x, event2.containerPoint.y];
            this.rectangle = L.rectangle([this.firstPointLatLng, this.firstPointLatLng], this.options.rectangle).addTo(this.map);
            this.map.on('mousemove', this.onMouseMove, this);
        }
    },

    onMouseMove(event: LeafletEvent) {
        if (this.rectangle) {
            const event2 = event as LeafletMouseEvent;
            this.lastPointLatLng = [event2.latlng.lat, event2.latlng.lng];
            this.lastPointPixel = [event2.containerPoint.x, event2.containerPoint.y];
            this.rectangle.setBounds([this.firstPointLatLng, this.lastPointLatLng]);
        }
    },

    onMouseUp() {
        if (this.lastPointLatLng) {
            // const selectedFeatures = this.getSelectedLayers(this.rectangle);

            const SIZE: [number, number] = [this.firstPointPixel[0] - this.lastPointPixel[0], this.firstPointPixel[1] - this.lastPointPixel[1]];
            if (SIZE[0] < 0) {
                SIZE[0] = this.lastPointPixel[0] - this.firstPointPixel[0];
            }
            if (SIZE[1] < 0) {
                SIZE[1] = this.lastPointPixel[1] - this.firstPointPixel[1];
            }

            this.map.fire('lasso.finished', {
                center: this.rectangle.getCenter(),
                size: SIZE,
                // layers: selectedFeatures,
            });

            this.map.removeLayer(this.rectangle);
            this.rectangle = undefined;
            this.firstPointLatLng = undefined;
            this.lastPointLatLng = undefined;

            this.disable();
        }
    },

    // getSelectedLayers(rectangle: L.Rectangle) {
    //     const lassoPolygonGeometry = rectangle.toGeoJSON().geometry;
    //
    //     const layers: L.Layer[] = [];
    //     this.map.eachLayer((layer: L.Layer) => {
    //         if (layer === this.rectangle) {
    //             return;
    //         }
    //
    //         if (L.MarkerCluster && layer instanceof L.MarkerCluster) {
    //             layers.push(...layer.getAllChildMarkers());
    //         } else {
    //             layers.push(layer);
    //         }
    //     });
    //
    //     const selectedLayers = layers.filter((layer) => {
    //         if (layer instanceof L.Marker) {
    //             const layerGeometry = layer.toGeoJSON().geometry;
    //             return booleanPointInPolygon(layerGeometry, lassoPolygonGeometry);
    //         }
    //         return false;
    //     });
    //
    //     return selectedLayers;
    // },
});

(L as any).Lasso = lasso;
(L as any).lasso = (map: L.Map, options: L.LassoOptions) => {
    return new L.Lasso(map, options);
};
