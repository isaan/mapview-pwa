import * as L from 'leaflet';
import 'leaflet-imageoverlay-rotated'; // To check: https://github.com/publiclab/Leaflet.DistortableImage
import { CustomMarker } from './custom-marker';

export class CustomImage {
    public _leafletImage: L.ImageOverlay.Rotated;
    public markers: CustomMarker[];

    private dataUrl: string;
    private opacity: number;
    private isVisible: boolean;

    constructor(marker1: CustomMarker, marker2: CustomMarker, marker3: CustomMarker, dataUrl: string) {
        this.markers = new Array();

        marker1.setSize(8);
        marker2.setSize(8);
        marker3.setSize(8);

        this.markers[0] = marker1;
        this.markers[1] = marker2;
        this.markers[2] = marker3;
        this.opacity = 0.4;
        this.isVisible = true;

        this.markers[0].onDrag(() => {
            this._leafletImage.reposition(this.markers[0].getLatLng(), this.markers[1].getLatLng(), this.markers[2].getLatLng());
        });
        this.markers[1].onDrag(() => {
            this._leafletImage.reposition(this.markers[0].getLatLng(), this.markers[1].getLatLng(), this.markers[2].getLatLng());
        });
        this.markers[2].onDrag(() => {
            this._leafletImage.reposition(this.markers[0].getLatLng(), this.markers[1].getLatLng(), this.markers[2].getLatLng());
        });

        this.dataUrl = dataUrl;
        this._leafletImage = L.imageOverlay.rotated(this.dataUrl, this.markers[0].getLatLng(), this.markers[1].getLatLng(), this.markers[2].getLatLng(), {
            opacity: this.opacity,
            interactive: true,
        });

        this.addListeners();
    }

    // ---
    // PUBLIC
    // ---

    public addTo(map: L.Map): void {
        this._leafletImage.addTo(map);
        map.addLayer(this._leafletImage);
    }

    public removeFrom(map: L.Map): void {
        map.removeLayer(this._leafletImage);
        this.markers[0].removeFrom(map);
        this.markers[1].removeFrom(map);
        this.markers[2].removeFrom(map);
    }

    public toggle(): void {
        if (this.isVisible) {
            this._leafletImage.setOpacity(0);
            this.markers[0].toggle();
            this.markers[1].toggle();
            this.markers[2].toggle();
            this.isVisible = false;
        } else {
            this._leafletImage.setOpacity(this.opacity);
            this.markers[0].toggle();
            this.markers[1].toggle();
            this.markers[2].toggle();
            this.isVisible = true;
        }
    }

    // ---
    // SETTERS
    // ---

    public setOpacity(opacity: number): void {
        this._leafletImage.setOpacity(opacity);
    }

    // ---
    // PRIVATE
    // ---

    private addListeners(): void {
        this._leafletImage.on('dblclick', (e: any) => {
            alert(e);
            alert('Double click on image.');
            e.stop();
        });

        this._leafletImage.on('click', () => {
            alert('Click on image.');
        });
    }
}
