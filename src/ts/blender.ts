export function blendColors(color1: string, color2: string, percentage: number): string {
    color1 = color1 || '#000000';
    color2 = color2 || '#ffffff';
    percentage = percentage || 0.5;

    const regexp: RegExp = new RegExp('^#[0-9a-f]{6}');
    if (! regexp.test(color1) || ! regexp.test(color2)) {
        throw new Error('colors must be provided as hexes');
    }

    if (percentage > 1 || percentage < 0) {
        percentage = 0.5;
    }

    color1 = color1.substring(1);
    color2 = color2.substring(1);

    const color1inArray: number[] = [parseInt(color1[0] + color1[1], 16), parseInt(color1[2] + color1[3], 16), parseInt(color1[4] + color1[5], 16)];
    const color2inArray: number[] = [parseInt(color2[0] + color2[1], 16), parseInt(color2[2] + color2[3], 16), parseInt(color2[4] + color2[5], 16)];

    const color3inArray: number[] = [
        (1 - percentage) * color1inArray[0] + percentage * color2inArray[0],
        (1 - percentage) * color1inArray[1] + percentage * color2inArray[1],
        (1 - percentage) * color1inArray[2] + percentage * color2inArray[2],
    ];

    const color3: string = '#' + int_to_hex(color3inArray[0]) + int_to_hex(color3inArray[1]) + int_to_hex(color3inArray[2]);
    return color3;
}

function int_to_hex(num: number): string {
    const hex: string = Math.round(num).toString(16);
    return hex.length === 1 ? '0' + hex : hex;
}
