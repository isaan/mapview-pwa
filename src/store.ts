import Vue from 'vue';
import Vuex from 'vuex';
import { CustomMap } from '@/ts/custom-map';

import * as CircularJSON from 'circular-json';

Vue.use(Vuex);

// https://vuex.vuejs.org/guide/state.html
// https://codeburst.io/vuex-store-d888de10d499
// https://forum.vuejs.org/t/best-practices-vuex-state-class-functions/22790/3
export default new Vuex.Store({
    // plugins: [
    //     (store) => {
    //         store.subscribe((mutation, state) => {
    //             console.log('Mutation detected.');
    //             localStorage.setItem('state', CircularJSON.stringify(state.map, (key: string, value: string) => {
    //                 const regexp: RegExp = new RegExp('^_.*$');
    //                 if (regexp.test(key)) {
    //                     return undefined;
    //                 }
    //                 return value;
    //             }));
    //             console.log(localStorage.getItem('state'));
    //         });
    //     },
    // ],
    state: {
        map: new CustomMap(),
    },
    getters: {
        // Compute derived state based on the current state. More like computed property.
    },
    mutations: {
        // Mutate the current state
        updateMap(state, model) {
            for (const prop in model) {
                if (state.map.hasOwnProperty(prop)) {
                    // @ts-ignore
                    state.map[prop] = model[prop];
                }
            }
        },
    },
    actions: {
        // Get data from server and send that to mutations to mutate the current state
    },
});
