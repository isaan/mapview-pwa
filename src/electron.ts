import * as path from 'path';
import { app, BrowserWindow } from 'electron';

let mainWindow: Electron.BrowserWindow | null;

// https://medium.com/@mikeal/vue-js-electron-the-easy-way-adc3ca09234a

function createWindow() {
    mainWindow = new BrowserWindow({
        width: 1400,
        height: 800,
        minWidth: 800,
        minHeight: 600,
    });

    mainWindow.loadFile(path.join(__dirname, '../src/index.html'));

    mainWindow.on('closed', () => {
        mainWindow = null;
    });

    mainWindow.once('ready-to-show', () => {
        if (mainWindow !== null) {
            mainWindow.show();
        }
    });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    if (mainWindow === null) {
        createWindow();
    }
});
